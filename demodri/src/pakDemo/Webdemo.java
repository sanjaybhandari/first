package pakDemo;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.junit.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Webdemo {

	@BeforeTest(alwaysRun = true)
	public void btest() {
		System.out.println("This is before Test");
	}

	@BeforeMethod
	public void bemethod() {
		System.out.println("This is before method");
	}

	@AfterTest
	public void afterTest() {
		System.out.println("after test");
	}

	@BeforeClass
	public void Beforclass() {
		System.out.println("Before class");
	}

	@Test(enabled = true)
	public void main() throws InterruptedException {

		WebDriver driver = new FirefoxDriver();

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("http://www.gmail.com");
		//driver.navigate().to("http://www.google.com");
		//driver.navigate().to("https://in.yahoo.com/");
		driver.findElement(By.cssSelector("#Email")).sendKeys("akshayshrma30@gmail.com");
		driver.findElement(By.cssSelector("[type='password']")).sendKeys("g()ess????");
		//driver.findElement(By.xpath("//input[@id='Email']")).sendKeys("akshayshrma30@gmail.com");
		///driver.findElement(By.xpath("//input[@id='Passwd']")).sendKeys("g()ess????");
	    driver.findElement(By.xpath("//input[@name='signIn']")).click();
	    driver.findElement(By.xpath("//a[@href='https://mail.google.com/mail/u/0/#drafts']")).click();
//	    a class="J-Ke n0"
	    driver.findElement(By.xpath("//table//tr[2]//div[@role='checkbox']")).click();
	    //table.F tr div[role*='checkbox']
	   // div class="T-Jo-auh"
		driver.findElement(By.cssSelector("table.F tr:nth-of-type(6) div[role*='link']")).click();
//		driver.findElement(By.xpath("//table//tr[5]//div[@role= 'link']")).click();
		//driver.findElement(By.id("Email")).sendKeys("akshayshrma30@gmail.com");
		//driver.findElement(By.xpath("//@class=''")).sendKeys("g()ess????");
		//input id="Passwd" class="" type="password" placeholder="Password" name="Passwd"
		//driver.findElement(By.id("Passwd")).sendKeys("g()ess????");
		// driver.findElement(By.id("PersistentCookie")).click();
		//driver.findElement(By.id("signIn")).click();
		// driver.findElement(By.id("smsUserPin")).sendKeys("173290");

		
		
		//String csstext = "table.F tr:nth-of-type({0}) div[role*='link']";
		// String csstext = "table.F tr:nth-of-type({0}) div[role*='link']";
		//driver.findElement(By.cssSelector("#nav-cricket")).click();
		//driver.findElement(By.cssSelector("[href='https://cricket.yahoo.com/postmatch-afghanistan-vs-scotland_190544']")).click();
		//String xpathtext = "//table//tr[{0}]//div[contains(@role, 'link')]";
		//driver.findElement(By.xpath("//a[class='d-b pl-l ell']")).click();
		//a class="d-b pl-l ell"
		//driver.findElement(By.cssSelector(".d-b pl-l ell")).click();
		//driver.findElement(By.xpath("//span[@id=':3q']")).click();            // (right)
		//Css Demo
	//driver.findElement(By.cssSelector(csstext.replace("{0}", "8"))).click();

		//Xpath Demo
		//driver.findElement(By.xpath(xpathtext.replace("{0}", "8"))).click();
		 //driver.findElement(By.cssSelector("table.F tr:nth-of-type({4}) div[role*='link']")).click();
		// driver.findElement(By.cssSelector("table.F td div[role*='link']");
		//driver.findElement(By.xpath("//tr[9]/td[6]/div/div/div/span")).click();
		//driver.findElement(By.xpath("")).click();
		// p:nth of type(2)
		// driver.navigate().to("http:\\www.google.com");
		// driver.get("http://www.google.com");
		 String title = driver.getTitle();
		  System.out.print(title);

		// driver.findElement(By.id("gbqfq")).sendKeys("Gmail");

		// driver.findElement(By.name("gbqfq")).sendKeys("gmail");
		// driver.findElement(By.linkText("Gmail - Google")).click();
		// driver.findElement(By.cssSelector(".rc-button")).click();

		// List li1=driver.findElements(By.className("gbqfif"));
		// List li2=driver.findElements(By.tagName("tr"));
		// int i=li1.size();
		// System.out.println("The size of list is = "+i);

		// Thread.sleep(5000);
		// String str=driver.findElement(By.id("gbqfq")).getAttribute("value");
		// System.out.println("The result  is= "+str);

		//driver.quit();

	}

	@Test(enabled = true)
	public void dispaly() {

		System.out.println("hi");

	}

	@Test(priority = 2)
	public void dis() {

		System.out.println("hellooooo");

	}

}
