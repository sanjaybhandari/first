package demodriv;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

public class befmet {
	@BeforeSuite
	public void m() {
		System.out.println("beforesuite");

	}

	@BeforeClass
	public void k() {
		System.out.println("beforeclasss");

	}

	@BeforeMethod
	public void c() {
		System.out.println("beforemethod");
	}

	@Test
	public void b() {
		System.out.println("test1");

	}

	@Test
	public void d() {
		System.out.println("test2");
	}

	@AfterMethod
	public void q() {
		System.out.println("aftermethod");
	}

	@AfterClass
	public void e() {
		System.out.println("afterclasss");

	}

	@AfterSuite
	public void g() {
		System.out.println("aftersuite");

	}

}
